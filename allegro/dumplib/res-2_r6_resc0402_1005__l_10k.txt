(DEVICE FILE: RES-2_R6_RESC0402_1005__L_10K)

PACKAGE RESC0402_1005__L
CLASS IC
PINCOUNT 2

PINORDER 'RES-2_R6_RESC0402_1005__L_10K' 1 2
FUNCTION G1 'RES-2_R6_RESC0402_1005__L_10K' 1 2

PACKAGEPROP PART_NAME 'RES-2_R6'
PACKAGEPROP VALUE 10k

END
