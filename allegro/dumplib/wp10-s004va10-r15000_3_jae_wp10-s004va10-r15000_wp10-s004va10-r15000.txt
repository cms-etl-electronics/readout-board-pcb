(DEVICE FILE: WP10-S004VA10-R15000_3_JAE_WP10-S004VA10-R15000_WP10-S004VA10-R15000)

PACKAGE 'JAE_WP10-S004VA10-R15000'
CLASS IC
PINCOUNT 14

PINORDER 'WP10-S004VA10-R15000_3_JAE_WP10' 1 2 3 4 P1 P2 P3 P4
FUNCTION G1 'WP10-S004VA10-R15000_3_JAE_WP10' 1 2 3 4 P1 P2 P3 P4

NC ; SH1 SH2 SH6 SH5 SH3 SH4

PACKAGEPROP PART_NAME 'WP10-S004VA10-R15000_3'
PACKAGEPROP VALUE 'WP10-S004VA10-R15000'

END
